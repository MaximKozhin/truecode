<?php

use yii\db\Migration;

/**
 * Class m200902_083109_lead
 */
class m200902_083109_lead extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lead}}', [
            'id'            => $this->primaryKey(10)->unsigned(),
            'name'          => $this->string()->notNull()->unique()->comment('Имя лида'),
            'source_id'     => $this->integer(10)->unsigned()->comment('Источник'),
            'status'        => $this->integer(1)->unsigned()->comment('Статус'),
            'deleted'       => $this->boolean()->defaultValue(0)->comment('Удалено'),
            'created_at'    => $this->integer(10)->unsigned()->comment('Создано'),
            'created_by'    => $this->integer(10)->unsigned()->comment('Кто создал'),
            'updated_at'    => $this->integer(10)->unsigned()->comment('Изменено'),
            'updated_by'    => $this->integer(10)->unsigned()->comment('Кто изменил'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->createIndex('index-lead-deleted', '{{%lead}}', 'deleted');

        $this->addForeignKey('fk-lead-created_by', '{{%lead}}', 'created_by', '{{%identity}}', 'id');
        $this->addForeignKey('fk-lead-updated_by', '{{%lead}}', 'updated_by', '{{%identity}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-lead-updated_by', '{{%lead}}');
        $this->dropForeignKey('fk-lead-created_by', '{{%lead}}');

        $this->dropTable('{{%lead}}');
    }
}
