<?php

$settings = require_once(__DIR__ . '/settings.php');
$params = require_once(__DIR__ . '/params.php');

$config = [
    'id' => 'truecode',
    'name' => 'True Code',
    'language' => 'ru_RU',
    'layout' => 'main',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gbFao3nx934Hn3Hb2VB0soUjq1vep56Gsa',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\components\User',
            'identityClass' => 'app\models\db\Identity',
            'loginUrl' => ['/auth/login'],
            'logoutUrl' => ['/auth/logout'],
            'enableAutoLogin' => true,
            'acceptableRedirectTypes' => ['text/html','application/xhtml+xml','*/*']
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'Europe/Moscow',
            'datetimeFormat' => 'php:d mm Y H:i:s',
            'dateFormat' => 'php:d mm Y',
            'timeFormat' => 'php:H:i:s',
            'sizeFormatBase' => 1000
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host={$settings['database']['server']};dbname={$settings['database']['dbname']}",
            'username'  => $settings['database']['dbuser'],
            'password'  => $settings['database']['dbpass'],
            'charset' => $settings['database']['charset'],
            'enableSchemaCache' => YII_ENV_DEV ? false : true
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '*'],
    ];
}

return $config;
