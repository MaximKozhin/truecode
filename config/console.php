<?php

$settings = require_once(__DIR__ . '/settings.php');
$params = require_once(__DIR__ . '/params.php');

$config = [
    'id' => 'truecode',
    'name' => 'True Code',
    'language' => 'ru_RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'user' => [
            'class' => 'app\components\User',
            'identityClass' => 'app\models\db\Identity',
            'loginUrl' => ['/main/auth/login'],
            'logoutUrl' => ['/main/auth/logout'],
            'enableAutoLogin' => true,
            'acceptableRedirectTypes' => ['text/html','application/xhtml+xml','*/*']
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host={$settings['database']['server']};dbname={$settings['database']['dbname']}",
            'username'  => $settings['database']['dbuser'],
            'password'  => $settings['database']['dbpass'],
            'charset' => $settings['database']['charset'],
            'enableSchemaCache' => YII_ENV_DEV ? false : true
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
