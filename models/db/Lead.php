<?php

namespace app\models\db;

use Yii;
use app\models\traits\NonDeletableTrait;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%lead}}".
 *
 * @property int $id
 * @property string $name Имя лида
 * @property int|null $source_id Источник
 * @property int|null $status Статус
 * @property int|null $deleted Удалено
 * @property int|null $created_at Создано
 * @property int|null $created_by Кто создал
 * @property int|null $updated_at Изменено
 * @property int|null $updated_by Кто изменил
 *
 * @property Identity $createdBy
 * @property Identity $updatedBy
 *
 * @property string|null $statusToString
 */
class Lead extends ActiveRecord
{
    use NonDeletableTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lead}}';
    }

    const STATUS_OPEN       = 1;
    const STATUS_CONTACT    = 2;
    const STATUS_OFFER      = 3;
    const STATUS_ANSWER     = 4;
    const STATUS_CONVERTED  = 5;
    const STATUS_REJECTED   = 7;

    /**
     * @return string[]
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_OPEN       => 'Новый',
            self::STATUS_CONTACT    => 'В обработке',
            self::STATUS_OFFER      => 'Предложение',
            self::STATUS_ANSWER     => 'Получение ответа',
            self::STATUS_CONVERTED  => 'Сконвертирован',
            self::STATUS_REJECTED   => 'Забракован',
        ];
    }

    /**
     * @return string|null
     */
    public function getStatusToString()
    {
        return key_exists($this->status, $statuses = self::getStatuses())
            ? $statuses[$this->status]
            : null;
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at'
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
                'value' => Yii::$app->user->getId()
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['source_id', 'status', 'deleted', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['status'], 'in', 'range' => array_keys(self::getStatuses())],
            [['status'], 'default', 'value' => self::STATUS_OPEN],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя лида',
            'source_id' => 'Источник',
            'status' => 'Статус',
            'deleted' => 'Удалено',
            'created_at' => 'Создано',
            'created_by' => 'Кто создал',
            'updated_at' => 'Изменено',
            'updated_by' => 'Кто изменил',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Identity::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Identity::class, ['id' => 'updated_by']);
    }
}
