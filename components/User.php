<?php

namespace app\components;

/**
 * Class LdapUser
 * @package app\components
 */
class User extends \yii\web\User
{
    /** @var array|string  */
    public $logoutUrl = ['site/logout'];
}