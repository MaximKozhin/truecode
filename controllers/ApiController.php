<?php

namespace app\controllers;

use app\models\db\Identity;
use app\models\db\Lead;
use yii\base\DynamicModel;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\IdentityInterface;
use yii\web\Response;

/**
 * Class ApiController
 * @package app\controllers
 */
class ApiController extends Controller
{
    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET'],
                    'statuses' => ['GET'],
                    'create' => ['POST'],
                ],
            ],
            'authenticator' => [
                'class' => HttpBasicAuth::class,
                'auth' => [$this, 'checkAccess']
            ],
            'rateLimiter' => [
                'class' => RateLimiter::class,
            ],
        ];
    }

    /**
     * Метод проверки доступа пользователя из базовой аутентификации
     *
     * @param $username
     * @param $password
     * @return IdentityInterface|null
     */
    public function checkAccess($username, $password)
    {
        if($identity = Identity::findOne(['username' => $username, 'deleted' => false])) {
            return $identity->validatePassword($password) ? $identity : null;
        }
        return null;
    }

    /**
     * @return array|void
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => Lead::class,
                'dataFilter' => [
                    'class' => 'yii\data\DataFilter',
                    'filterAttributeName' => 'filter',
                    'searchModel' =>  function () {
                         return (new DynamicModel(['status',' source_id', 'created_by']))
                                ->addRule('source_id', 'integer')
                                ->addRule('created_by', 'integer')
                                ->addRule('status', 'in', ['range' => array_keys(Lead::getStatuses())]);
                         },
                ]
            ],
            'create' => [
                'class' => 'yii\rest\CreateAction',
                'modelClass' => Lead::class,
            ],
            'statuses' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => Lead::class,
                'prepareDataProvider' => function () {
                    return Lead::getStatuses();
                }
            ],
            'identities' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => Identity::class,
                'dataFilter' => [
                    'class' => 'yii\data\DataFilter',
                    'filterAttributeName' => 'filter',
                    'searchModel' =>  function () {
                        return (new DynamicModel(['deleted', 'username']))
                            ->addRule('deleted', 'boolean')
                            ->addRule('username', 'string');
                    },
                ]
            ]
        ];
    }
}