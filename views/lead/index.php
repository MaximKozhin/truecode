<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">
    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
        <div class="pull-right">
            <a href="<?=Url::to(['create'])?>" class="btn btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                <span class="hidden-xs hidden-sm">Добавить лида</span>
            </a>
        </div>
    </h1>
    <hr>
    <div style="overflow: auto;">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model){
                return $model->deleted ? ['class' => 'danger'] : [];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',
                'source_id',
                [
                    'attribute' => 'status',
                    'value' => 'statusToString',
                ],
                'deleted:boolean',
                'created_at:datetime',
                [
                    'attribute' => 'created_by',
                    'value' => 'createdBy.username',
                ],
                'updated_at:datetime',
                [
                    'attribute' => 'updated_by',
                    'value' => 'updatedBy.username',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
