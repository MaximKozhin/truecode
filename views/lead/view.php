<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Lead */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-view">

    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
        <div class="pull-right">
            <a href="<?=Url::to(['update', 'id' => $model->id])?>" class="btn btn-primary">
                <span class="glyphicon glyphicon-pencil"></span>
                <span class="hidden-xs hidden-sm">Редактировать</span>
            </a>
            <a href="<?=Url::to(['delete', 'id' => $model->id])?>" class="btn btn-danger" data-method="POST" data-confirm="Удалить?">
                <span class="glyphicon glyphicon-trash"></span>
                <span class="hidden-xs hidden-sm">Удалить</span>
            </a>
        </div>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'source_id',
                [
                    'attribute' => 'status',
                    'value' => $model->statusToString,
                ],
                'deleted:boolean',
                'created_at:datetime',
                [
                    'attribute' => 'created_by',
                    'value' => $model->createdBy->username,
                ],
                'updated_at:datetime',
                [
                    'attribute' => 'updated_by',
                    'value' => $model->updatedBy->username,
                ],
            ],
        ]) ?>
    </div>

</div>
